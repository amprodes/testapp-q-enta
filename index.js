// Importing dependencies
let express 	   = require('express'), 
	  bodyParser   = require('body-parser'), 
	  cors 		     = require('cors'),    
      path 		   = require('path'),  
	  createError  = require('http-errors')

// Initialize the app
let app = express() 

// view engine setup 
app.set('views', path.join(__dirname, 'views'));  
//app.set('views', [path.join(__dirname, 'views/admin/'), path.join(__dirname, 'views')])
app.set('view engine', 'pug');

// Import routes
let webRoutes = require("./router/web-routes"),
    apiRoutes = require("./router/api-routes")
	
// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}))
 
//app.use(session({secret: 'sdofhwf98y23r',saveUninitialized: true,resave: true, cookie: { secure: true }}));
app.use(bodyParser.json())
app.use(cors()) 
 
app.use(express.json())
app.use(express.urlencoded({ extended: false })) 
app.use(express.static(path.join(__dirname, 'public'))) 

// Setup server port
var port = `1884`;

// Send message for default URL
app.use('/', webRoutes);

app.use('/api', apiRoutes);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404))
})

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
})

// Launch app to listen to specified port
app.listen(port, function () {
    console.log("Running APP on port " + port);
});