$(document).ready(function() {
  'use strict'  

  let url        = window.location.href,
      urlLoading = url.split('/') 

        $(".dynamicScript").append('<script type="text/javascript" src="../../lib/datatables.net/js/jquery.dataTables.min.js"></script>');
        $(".dynamicScript").append('<script type="text/javascript" src="../../lib/datatables.net-dt/js/dataTables.dataTables.min.js"></script>');
        $(".dynamicScript").append('<script type="text/javascript" src="../../lib/datatables.net-responsive/js/dataTables.responsive.min.js"></script>');
        $(".dynamicScript").append('<script type="text/javascript" src="../../lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js"></script>');
         

          $('#players').DataTable({
            ajax: '/api/players/list',
            "columns": [
            {"data" : "first_name"},
            {"data" : "last_name"},
            {"data" : "height_feet"},
            {"data" : "height_inches"}, 
            {"data" : "position"},
            {"data" : "team"},
            {"data" : "weight_pounds"}
            ],
            "columnDefs": [{
              "targets": 0,
              "data": "name",
              "render": function ( data, type, row, meta ) {
                return `<a href="player/${row.id}">${data}</a>`;
              }
            }],
              language: {
                loadingRecords: "Cargando jugadores...",
                searchPlaceholder: 'Buscar...',
                emptyTable:     "No se encontraron jugadores",
                sSearch: '',
                lengthMenu: '_MENU_ items/pagina',
              paginate: {
                first:      "Primero",
                previous:   "Anterior",
                next:       "Siguiente",
                last:       "Ultimo"
              },
              info:         "Mostrando de _START_ a _END_ de un total de _TOTAL_ jugadores"
            }
          })

          //edit player 
          if(typeof player != 'undefined'){
            $('form#edit_player').append(`<input type="hidden" name="id" id="id" value="${player.id}">`)
            $('#first_name').val(player.first_name)
            $('#height_feet').val(player.height_feet)
            $('#height_inches').val(player.height_inches)
            $('#last_name').val(player.last_name) 
            $('#position').val(player.position)
            $('#team').val(player.team) 
            $('#weight_pounds').val(player.weight_pounds) 

            $( "#edit_player" ).submit(function(event) {
              event.preventDefault()

              let url,
                  data

              if(typeof player != 'undefined'){
                url  =  `/api/player/${player.id}` 
              }else{
                url  =  `/api/player/add`
              } 

              let first_name    = $('#first_name').val(),
                  height_feet   = $('#height_feet').val(),
                  height_inches = $('#height_inches').val(),
                  last_name     = $('#last_name').val(),
                  position      = $('#position').val(),
                  team          = $('#team').val(),
                  weight_pounds = $('#weight_pounds').val(),
                  id            = $('#id').val()

                $.ajax({
                  type: 'POST',
                  url,
                  data : { first_name, height_feet, height_inches, last_name, position, team, weight_pounds, id},
                  success: function(response){

                        if(response.error){
                          let html = "<div class='alert alert-solid alert-danger text-center' role='alert'>"+response.error+"</div>"
                          $('.message').prepend(html)
                       }else{
                          let html = "<div class='alert alert-solid alert-success text-center' role='alert'>"+response.message+"</div>"
                          
                          if(typeof player == 'undefined'){ 
                            $('#add_visitors')[0].reset()
                          }

                          $('.message').prepend(html) 

                       }

                    },
                    error: function() {
                    }

                  })
            }) 
          }
}) 