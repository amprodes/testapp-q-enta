// api-routes.js
// Initialize express router
let router         = require('express').Router()

// Set default API response
router.get('/', function (req, res) { 
    res.json({
        status: 'APP routes Its Working',
        message: 'Welcome to test APP!',
    })
})

// Import guest controller
var playerController     = require('../controllers/player')

// Get Players
router.route('/players/list').get(playerController.list)
//router.route('/player/:id').get(playerController.read)
router.route('/player/:id').post(playerController.update)

// Export API routes
module.exports = router;